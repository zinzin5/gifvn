<?php
namespace App\Library\ImageServiceStorage;
class Imgur{
    public static function uploadImage($img){
        try{
            $tmp_url = $img->getTempName();
            $client_id="b718061d9719988";
            $handle = fopen($tmp_url, "r");
            $data = fread($handle, filesize($tmp_url));
            $pvars   = array('image' => base64_encode($data));
            $timeout = 10;
            unlink($tmp_url);
            //using curl to post image
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
            curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
            $out = curl_exec($curl);
            curl_close ($curl);
            $pms = json_decode($out,true);
            $result = array();
            if ($pms['success']){
                $result['error'] = 0;
                $result['url'] = $pms['data']['link'];
                return $result;
            } else{
                $result['error'] = 1;
                $result['message'] = $pms['data']['error'];
                return $result;
            }            
        }
        catch(Exception $e){
            throw new Exception($e->getMessage());
            return null;
        }
    }
}

?>