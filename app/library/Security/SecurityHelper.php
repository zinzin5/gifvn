<?php
namespace App\Library\Security;
class SecurityHelper{
	/**
	 * Encrypt password
	 * @param  [string] $password [origin password]
	 * @return [string]           [encrypted password]
	 */
	public static function encryptPassword($password){
		return md5($password);
	}

	public static function verifyPassword($needVerifyPassword, $password){
		return (strcmp(md5($needVerifyPassword), $password) == 0)?true:false;
	}
}