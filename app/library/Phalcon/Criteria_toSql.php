<?php
	namespace App\Library\Phalcon;
	use ReflectionClass;
	class Criteria_toSql {
		
		function __construct(){
			
		}

		public static function toSql($object){
			//get _result object  (return object)
			$reflectionClass = new ReflectionClass($object);
			$reflectionProperty = $reflectionClass->getProperty('_result');
			$reflectionProperty->setAccessible(true);
			$property = $reflectionProperty->getValue($object);

			//_sqlStatement	(return string)
			$reflectionClass = new ReflectionClass($property);
			$reflectionProperty = $reflectionClass->getProperty('_sqlStatement');
			$reflectionProperty->setAccessible(true);
			$sqlStatement = $reflectionProperty->getValue($property);

			//get bindData
			$reflectionProperty = $reflectionClass->getProperty('_bindParams');
			$reflectionProperty->setAccessible(true);
			$bindParams = $reflectionProperty->getValue($property);

			foreach ($bindParams as $key => $value){
				if (gettype($value) != 'integer'){
					$sqlStatement = str_replace(":{$key}" ,'\'' . $value . '\'' , $sqlStatement);	
				}
				else{
					$sqlStatement = str_replace(":{$key}" ,"{$value}" , $sqlStatement);	
				}
			}
			

			return $sqlStatement;
		}
	}
	
?>