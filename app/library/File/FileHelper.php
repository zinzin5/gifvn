<?php
namespace App\Library\File;
class FileHelper{

	const RESOURCE_PATH_TO_USER_AVATAR_FOLDER = 'image/user/avatar/';
	const RESOURCE_PATH_TO_DOCTOR_AVATAR_FOLDER = 'image/doctor/avatar/';
	const RESOURCE_PATH_TO_USER_THUMB_AVATAR_FOLDER = 'image/user/thumb_avatar/';
	const RESOURCE_PATH_TO_COMPANY_ICON_FOLDER = 'image/company/icon/';
	const RESOURCE_PATH_TO_PROJECT_ICON_FOLDER = 'image/project/icon/';

	/* --------------------------------------------- HELPER ------------------------------------------------------ */
	/**
	 * Create the folder
	 * @param  [string] $name [name of folder]
	 * @param  [string] $url  [url of folder]
	 * @return [bool]       [create result]
	 */
	// public static function createFolder($name, $url){
	// 	if (!is_dir($url.'/'.$name)) {
	// 	    return mkdir($url.'/'.$name, 0777, true);
	// 	}
	// 	return false;
	// }

	public static function createFolder($url){
		if (!is_dir($url)) {
		    return mkdir($url, 0777, true);
		}
		return false;
	}	
	
	/**
	 * Get the size of file in text format
	 * @param  [float] $size [file size in byte]
	 * @return [string]       [size in text format]
	 */
	public static function getFileSizeInTextFormat($size){
		if($size >= 1024 * 1024 * 1024){
			return number_format(($size/(1024 * 1024 * 1024)),0,'.','').' gb';
		}
		else if($size >= 1024 * 1024){
			return number_format(($size/(1024 * 1024)),0,'.','').' mb';
		}
		else if($size >= 1024){
			return number_format(($size/1024),0,'.','').' kb';
		}
		else{
			return ($size).' b';
		}
	}

	public static function getTempName($params){
		return $params['tmp_name'];
	}

	/**
	 * Check if the file have extensions in the allow range
	 * @param  [stdObject] $params     [file data]
	 * @param  [array] $extensions [array of the extensions accepted]
	 * @return [bool]             [true if file have extension in accepted range]
	 */
	public static function checkFileExtension($params, $extensions){
		//convert all extensions to undercase
		$newExtensions = array();
		foreach($extensions as $extension){
			array_push($newExtensions, strtolower($extension));
		}

		if(!is_null($newExtensions) && count($newExtensions) > 0){
			if ($params["error"] !== UPLOAD_ERR_OK) {
				throw new Exception('An error occurred when upload file.');
				return false;
			}

			$parts = pathinfo($params["name"]);
			if(isset($parts["extension"])){
				return in_array(strtolower($parts["extension"]), $newExtensions);
			}
			else{
				return false;
			}
		}
		else{
			return true;
		}
	}
	
	/**
	 * Take care of the upload file
	 * @param  [stdObject]  $params         [File data]
	 * @param  [string]  $path           [path to save the file]
	 * @param  boolean $keepFileName   [if true, keep the file name intact, otherwise, give it a random name]
	 * @param  integer $autoNameLength [length of random name]
	 * @param  boolean $override       [if true, will override the file if duplicate happen]
	 * @return [string]                  [name of file after saved]
	 */
	public static function handleFileUpload($params, $path, $keepFileName = false, $autoNameLength = 20, $override = false){
		if ($params["error"] !== UPLOAD_ERR_OK) {
			throw new Exception('An error occurred when upload file.');
			return;
		}
		
		if($keepFileName){
			// ensure a safe filename
			$name = preg_replace("/[^A-Z0-9._-]/i", "_", $params["name"]);
		}
		else{
			$parts = pathinfo($params["name"]);
			$name = StringHelper::generateRandomString($autoNameLength). "." . $parts["extension"];
		}
		
		if(!$override){
			$i = 0;
			$parts = pathinfo($name);
			while (file_exists($path . $name)) {
				$i++;
				$name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
			}
		}
		else{
			$name = $parts["filename"] . "." . $parts["extension"];
		}
		
		// preserve file from temporary directory
		try{
			if(!is_writable(dirname($path))){
				throw new Exception('Permission denied for upload folder.');
				return;
			}
			$success = move_uploaded_file($params["tmp_name"], $path . $name);
			if (!$success) {
				throw new Exception('Unable to save file.');
				return;
			}
		}
		catch(Exception $e){
			throw new Exception($e->getMessage());
			return;
		}
		
		return $name;
	}

	/* --------------------------------------------- USER AVATAR ------------------------------------------------------ */

	public static function createUserAvatarFolder(){
		return FileHelper::createFolder(self::RESOURCE_PATH_TO_USER_AVATAR_FOLDER);
	}

	public static function createUserAvatarThumbFolder(){
		return FileHelper::createFolder(self::RESOURCE_PATH_TO_USER_THUMB_AVATAR_FOLD);
	}
	
	public static function getUserAvatarUrl($filename, $absolute = true){
		return FileHelper::getUserAvatarFolderUrl($absolute).$filename;
	}

	public static function getUserAvatarFolderUrl($absolute = true){
		FileHelper::createUserAvatarFolder();
		return self::RESOURCE_PATH_TO_USER_AVATAR_FOLDER;
	}

	public static function getUserAvatarThumbUrl($filename, $absolute = true){
		return FileHelper::getUserAvatarThumbFolderUrl($absolute).$filename;
	}

	public static function getUserAvatarThumbFolderUrl($absolute = true){
		FileHelper::createUserAvatarThumbFolder();
		return self::RESOURCE_PATH_TO_USER_THUMB_AVATAR_FOLDER;
	}

	/* --------------------------------------------- TEMP ------------------------------------------------------ */

	/* get the url to the folder where we store prodcut image */
	public static function getPosUserTempFolderUrl($tenant, $absolute = true){
		FileHelper::createPosUserTempFolder($tenant);
		return '/temp/'.$tenant.'/';
	}

	/**
	 * Save upload file to temp folder with random name
	 */
	public static function saveToTemp($tenant, $params){
		$tempPath = FileHelper::getPosUserTempFolderUrl($tenant, false);

		if ($params["error"] !== UPLOAD_ERR_OK) {
			throw new Exception('An error occurred when upload file.');
			return;
		}
		
		$parts = pathinfo($params["name"]);
		$name = StringHelper::generateRandomString(20). "." . $parts["extension"];
		
		// preserve file from temporary directory
		try{
			if(!is_writable(dirname($tempPath))){
				throw new Exception('Permission denied for temp folder.');
				return;
			}
			$success = move_uploaded_file($params["tmp_name"], $tempPath . $name);
			if (!$success) {
				throw new Exception('Unable to save temp file.');
				return;
			}
		}
		catch(Exception $e){
			throw new Exception($e->getMessage());
			return;
		}
		
		return $tempPath . $name;
	}
}