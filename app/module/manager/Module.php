<?php

namespace App\Module\Manager;

use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Db\Adapter\Pdo\Mysql as MySQLAdapter;
use Phalcon\Config\Adapter\Ini as ConfigIni;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new Loader();
		$loader->registerDirs(
		    array(
		    	//register for controller and model of manager module
		        '../app/module/manager/controller/',
		        '../app/module/manager/model/',   

		    	//register base model of api module
		        '../app/module/api/model/_base',     
		    )
		);
		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices($di)
	{

		/**
		 * Read configuration
		 */
		$config = new ConfigIni(__DIR__ . "/config/config.ini");

		//Registering a dispatcher
		$di->set('dispatcher', function () {
			$dispatcher = new Dispatcher();
			return $dispatcher;
		});

		//Registering the view component
		$di->set('view', function () {
			$view = new \Phalcon\Mvc\View(true);
			return $view;
		});

		$di->set('db', function () use ($config) {
			return new MySQLAdapter(array(
				"adapter" => $config->database->adapter,
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name,
				'charset' => $config->database->charset,
			));
		});
	}
}
