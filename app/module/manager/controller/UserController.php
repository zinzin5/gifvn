<?php
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\CriteriaInterface;
use App\Module\Manager\Model\User as User;
class UserController extends Controller
{
	public function fetchUsersApiAction()
	{
		try {
			$postData = json_encode($_GET);
			$postParams = json_decode($postData);

			$query = User::query();
			$params = array();
			
			$query->limit(15);

			$query->bind($params);
			$user = $query->execute();

			$result = array();
			foreach ($user as $key => $value){
				array_push($result, $value->toArray());	
			}
			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
		}
	}
}
