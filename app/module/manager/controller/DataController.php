<?php
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\CriteriaInterface;

class DataController extends Controller
{
	public function getNewDataApiAction()
	{
		try {
			$postData = json_encode($_GET);
			$postParams = json_decode($postData);

			$query = Data::query();
			$params = array();
			
			$query->limit(15);
			if (property_exists($postParams, 'type') && isset($postParams->type)){
				$data['c_type'] = $postParams->type;
                $query->andWhere("c_type = :type:");
				$params["type"] = $data['c_type'];
			}
			$query->bind($params);
			$list_data = $query->execute();

			$result = array();
			foreach ($list_data as $key => $value){
				array_push($result, $value->toArray());	
			}
			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
		}
	}
}
