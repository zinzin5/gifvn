<?php

namespace App\Modules\Api;

use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Db\Adapter\Pdo\Mysql as MySQLAdapter;
use Phalcon\Config\Adapter\Ini as ConfigIni;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new Loader();
		$loader->registerDirs(
		    array(
		        '../apps/modules/error/controllers/',
		        '../apps/modules/error/models/',
		    )
		);
		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices($di)
	{

		/**
		 * Read configuration
		 */
		$config = new ConfigIni(__DIR__ . "/config/config.ini");

		//Registering a dispatcher
		$di->set('dispatcher', function () {
			$dispatcher = new Dispatcher();
			return $dispatcher;
		});

		//Registering the view component
		$di->set('view', function () {
			$view = new \Phalcon\Mvc\View(true);
			return $view;
		});

		$di->set('db', function () use ($config) {
			return new MySQLAdapter(array(
				"adapter" => $config->database->adapter,
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name,
				'charset' => $config->database->charset,
			));
		});
	}
}
