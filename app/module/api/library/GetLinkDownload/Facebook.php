<?php
	namespace App\Module\Api\Library\GetLinkDownload;

	class Facebook {

		function __construct() {
			
		}

		public static function get_link_image($data){
	      	$result = array();
	    	array_push($result, $data['c_url_full_size']);
	    	return $result;
	    }

		public static function get_link_video($data){
			$link_download = self::process_source_content($data['c_source_link']);
			$code_video_facebook = self::code_video_facebook();
			$result = array();
			foreach ($link_download as $key => $value){
	    		if (isset($code_video_facebook[$key]) && ($code_video_facebook[$key]["active"])){
		    		$link['url'] = $value;
		    		$link['resolution'] = $code_video_facebook[$key]["resolution"];	
		    		$link['type'] = $code_video_facebook[$key]["type"];	
		    		array_push($result, $link);	
	    		}
	    	} 
	    	return $result;
	    }

		public static function process_source_content($link){
	      	if(substr($link, -1) != '/' && is_numeric(substr($link, -1))){
		        $link = $link.'/';
		    }
		    preg_match('/https:\/\/www.facebook.com\/(.*)\/videos\/(.*)\/(.*)\/(.*)/U', $link, $id); // link dạng https://www.facebook.com/userName/videos/vb.IDuser/IDvideo/?type=2&theater
		    if(isset($id[4])){
		        $idVideo = $id[3];
		    }else{
		        preg_match('/https:\/\/www.facebook.com\/(.*)\/videos\/(.*)\/(.*)/U', $link, $id); // link dạng https://www.facebook.com/userName/videos/IDvideo
		        if(isset($id[3])){
		            $idVideo = $id[2];
		        }else{
		            preg_match('/https:\/\/www.facebook.com\/video\.php\?v\=(.*)/', $link, $id); // link dạng https://www.facebook.com/video.php?v=IDvideo
		            $idVideo = $id[1];
		            $idVideo = substr($idVideo, 0, -1);
		        }
		    }
		    $embed = 'https://www.facebook.com/video/embed?video_id='.$idVideo; // đưa link về dạng embed
		    $get = self::curl($embed);
		    $data = explode('[["params","', $get); // tách chuỗi [["params"," thành mảng
		    $data = explode('"],["', $data[1]); // tách chuỗi "],[" thành mảng
		    // echo "1";	    
		    $data = str_replace(
		        array('\u00257B', '\u002522', '\u00253A', '\u00252C', '\u00255B', '\u00255C\u00252F', '\u00252F', '\u00253F', '\u00253D', '\u002526'),
		        array('{', '"', ':', ',', '[', '\/', '/', '?', '=', '&'),
		        $data[0]
		    ); // thay thế các ký tự mã hóa thành ký tự đặc biệt
		    //Link HD
		    $HD = explode('[{"hd_src":"', $data);
		    if (isset($HD[1])){
			    $HD = explode('","', $HD[1]);	
		    	$HD = str_replace('\/', '/', $HD[0]);
		    } else{
		    	$HD  = null;
		    }

		    //Link SD
		    $SD = explode('"sd_src":"', $data);
		    if (isset($SD[1])){
			    $SD = explode('","', $SD[1]);
		    	$SD = str_replace('\/', '/', $SD[0]);		    	
		    } else{
		    	$SD  = null;
		    }

		    if($HD){
		        $linkDownload['HD'] = $HD; // link download HD
		    }
		    if($SD){
		        $linkDownload['SD'] = $SD; // link download SD
		    }
		    
		    $imageVideo = 'https://graph.facebook.com/'.$idVideo.'/picture'; // get ảnh thumbnail
		    $linkVideo = array_values($linkDownload);
		    $return['linkVideo'] = $linkVideo[0]; // link video có độ phân giải lớn nhất
		    $return['imageVideo'] = $imageVideo; // ảnh thumb của video
		    $return['linkDownload'] = $linkDownload; // link download video
		    return $linkDownload;
	    }

	    public static function curl($url) {
		    $ch = @curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    $head[] = "Connection: keep-alive";
		    $head[] = "Keep-Alive: 300";
		    $head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		    $head[] = "Accept-Language: en-us,en;q=0.5";
		    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		    $page = curl_exec($ch);
		    curl_close($ch);
		    return $page;
		}

	    public static function code_video_facebook(){
	    	$code = array(
	    		"SD" 		=>	array("resolution" => "240p", "type" => "mp4", "active" => true),
	    		"HD" 		=>	array("resolution" => "720p", "type" => "mp4", "active" => true),

    		);
    		return $code;
	    }
	}
	
?>
