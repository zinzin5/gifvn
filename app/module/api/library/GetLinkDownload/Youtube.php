<?php
	namespace App\Module\Api\Library\GetLinkDownload;
	use Redis;
	use App\Config\AppConstant as AppConstant;

	class Youtube {
		
		function __construct() {
			
		}

		public static function get_link_video($data){
	    	$url = $data['c_source_link'];
	    	$parts = parse_url($url);
	    	$query = array();
	    	//handle with youtu.be.com domain
	    	if ($parts['host'] == 'youtu.be'){
	    		$query['v'] = substr($parts['path'], 1);
	    	} else{
	    		parse_str($parts['query'], $query);	
	    	}

	    	$redis = new Redis();
	    	if ($redis->connect('127.0.0.1', 6379) && $redis->auth('zinzin')&& $redis->select(0)){
	    		if ($redis->get('youtube_video_id:' . $query['v'])){
					$result = json_decode($redis->get('youtube_video_id:' . $query['v']));
	    		}
	    		else{
	    			$code_video_youtube = self::code_video_youtube();
					$link_download_content = file_get_contents('http://139.162.21.103:8080/2015_v1/link_2.php?video_id=' . $query['v']); 	    	
			    	$link_download_content = json_decode($link_download_content);
			    	$result = array();
			    	foreach ($link_download_content->url as $key => $value){
			    		if (isset($code_video_youtube[intval($key)]) && ($code_video_youtube[intval($key)]["active"])){
				    		$link['url'] = $value;
				    		$link['resolution'] = $code_video_youtube[intval($key)]["resolution"];	
				    		$link['type'] = $code_video_youtube[intval($key)]["type"];	
				    		array_push($result, $link);	
			    		}
			    	} 
			    	$redis->set('youtube_video_id:' . $query['v'], json_encode($result));
			    	$redis->setTimeout('youtube_video_id:' . $query['v'], AppConstant::TIME_EXPIRED_YOUTUBE); 		
	    		}
	    	}
    		else{
    			$code_video_youtube = self::code_video_youtube();
				$link_download_content = file_get_contents('http://139.162.21.103:8080/2015_v1/link_2.php?video_id=' . $query['v']); 	    	
		    	$link_download_content = json_decode($link_download_content);
		    	$result = array();
		    	foreach ($link_download_content->url as $key => $value){
		    		if (isset($code_video_youtube[intval($key)]) && ($code_video_youtube[intval($key)]["active"])){
			    		$link['url'] = $value;
			    		$link['resolution'] = $code_video_youtube[intval($key)]["resolution"];	
			    		$link['type'] = $code_video_youtube[intval($key)]["type"];	
			    		array_push($result, $link);	
		    		}
		    	} 
    		}   	   		
	    	return $result;
	    }

	    public static function code_video_youtube(){
	    	$code = array(
	    		5 		=>	array("resolution" => "240p", "type" => "flv", "active" => false),
	    		17 		=>	array("resolution" => "144p", "type" => "3gp", "active" => true),
	    		18 		=>	array("resolution" => "360p", "type" => "mp4", "active" => true),
	    		22 		=>	array("resolution" => "720p", "type" => "mp4", "active" => true),
	    		34 		=>	array("resolution" => "360p", "type" => "flv", "active" => false),
	    		35 		=>	array("resolution" => "480p", "type" => "flv", "active" => false),
	    		36 		=>	array("resolution" => "240p", "type" => "3gp", "active" => false),
	    		37 		=>	array("resolution" => "1080p", "type" => "mp4", "active" => true),
	    		38 		=>	array("resolution" => "1080p", "type" => "mp4", "active" => false),
	    		43 		=>	array("resolution" => "360p", "type" => "web", "active" => false),
	    		44 		=>	array("resolution" => "480p", "type" => "web", "active" => false),
	    		45 		=>	array("resolution" => "720p", "type" => "web", "active" => false),
	    		46 		=>	array("resolution" => "1080p", "type" => "web", "active" => false),
	    		82 		=>	array("resolution" => "360p", "type" => "mp4", "active" => true),
	    		83 		=>	array("resolution" => "480p", "type" => "mp4", "active" => false),
	    		84 		=>	array("resolution" => "720p", "type" => "mp4", "active" => true),
	    		85 		=>	array("resolution" => "1080p", "type" => "mp4", "active" => false),
	    		100 	=>	array("resolution" => "360p", "type" => "web", "active" => false),
	    		101 	=>	array("resolution" => "480p", "type" => "web", "active" => false),
	    		102 	=>	array("resolution" => "720p", "type" => "web", "active" => false),
	    		133 	=>	array("resolution" => "240p", "type" => "mp4", "active" => false),
	    		134 	=>	array("resolution" => "360p", "type" => "mp4", "active" => true),
	    		135 	=>	array("resolution" => "480p", "type" => "mp4", "active" => false),
	    		136 	=>	array("resolution" => "720p", "type" => "mp4", "active" => true),
	    		137 	=>	array("resolution" => "1080p", "type" => "mp4", "active" => true),
	    		139 	=>	array("resolution" => "Low bitrate", "type" => "mp4", "active" => false),
	    		140		=>	array("resolution" => "Med bitrate", "type" => "mp4", "active" => false),
	    		141		=>	array("resolution" => "Hi  bitrate", "type" => "mp4", "active" => false),
	    		160 	=>	array("resolution" => "144p", "type" => "mp4", "active" => true),
	    		171		=>	array("resolution" => "Med bitrate", "type" => "web", "active" => false),
	    		172		=>	array("resolution" => "Hi  bitrate", "type" => "web", "active" => false),
	    		242 	=>	array("resolution" => "240p", "type" => "web", "active" => false),
	    		243 	=>	array("resolution" => "360p", "type" => "web", "active" => false),
	    		244		=>	array("resolution" => "480p", "type" => "web", "active" => false),
	    		245 	=>	array("resolution" => "480p", "type" => "web", "active" => false),
	    		246 	=>	array("resolution" => "480p", "type" => "web", "active" => false),
	    		247 	=>	array("resolution" => "720p", "type" => "web", "active" => false),
	    		248 	=>	array("resolution" => "1080p", "type" => "web", "active" => false),
	    		264 	=>	array("resolution" => "1440p", "type" => "mp4", "active" => false),
	    		266 	=>	array("resolution" => "2160p", "type" => "mp4", "active" => false),
	    		271 	=>	array("resolution" => "1440p", "type" => "web", "active" => false),
	    		272 	=>	array("resolution" => "2160p", "type" => "web", "active" => false),
    		);
    		return $code;
	    }
	}
	/*  REFERENT
	itag=  video  resolution/bitrate
	value  type    ( w x h )  flags
	=====  =====  ==================
	   5    FLV    320 x 240
	  17    3GP    176 x 144              17    3GP    176 x 144
	  18    MP4    480 x 360              18    MP4    480 x 360
	  22    MP4   1280 x 720
	  34    FLV    480 x 360
	  35    FLV    640 x 480 
	  36    3GP    320 x 240
	  37    MP4   1920 x 1080             37    MP4   1920 x 1080
	  38    MP4   2048 x 1080             38    MP4   2048 x 1080
	  43    WEB    480 x 360
	  44    WEB    640 x 480
	  45    WEB   1280 x 720
	  46    WEB   1920 x 1080
	  82    MP4    480 x 360   3D         82    MP4    480 x 360   3D         
	  83    MP4    640 x 480   3D         83    MP4    640 x 480   3D
	  84    MP4   1280 x 720   3D         84    MP4   1280 x 720   3D
	  85    MP4   1920 x 1080  3D         85    MP4   1920 x 1080  3D       
	 100    WEB    480 x 360   3D
	 101    WEB    640 x 480   3D
	 102    WEB   1280 x 720   3D
	 133    MP4    320 x 240   VO         33    MP4    320 x 240   VO
	 134    MP4    480 x 360   VO         134    MP4    480 x 360   VO
	 135    MP4    640 x 480   VO         135    MP4    640 x 480   VO
	 136    MP4   1280 x 720   VO         136    MP4   1280 x 720   VO
	 137    MP4   1920 x 1080  VO         137    MP4   1920 x 1080  VO
	 139    MP4   Low bitrate  AO
	 140    MP4   Med bitrate  AO
	 141    MP4   Hi  bitrate  AO
	 160    MP4    256 x 144   VO
	 171    WEB   Med bitrate  AO
	 172    WEB   Hi  bitrate  AO
	 242    WEB    320 x 240   VOX
	 243    WEB    480 x 360   VOX
	 244    WEB    640 x 480   VOX
	 245    WEB    640 x 480   VOX
	 246    WEB    640 x 480   VOX
	 247    WEB   1280 x 720   VOX
	 248    WEB   1920 x 1080  VOX
	 264    MP4   2560 x 1440  VO       264    MP4   2560 x 1440  VO
	 266    MP4   3840 x 2160  VO       266    MP4   3840 x 2160  VO
	 271    WEB   2560 x 1440  VOX
	 272    WEB   3840 x 2160  VOX
	*/
?>
