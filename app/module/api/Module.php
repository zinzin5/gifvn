<?php

namespace App\Module\Api;

use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Db\Adapter\Pdo\Mysql as MySQLAdapter;
use Phalcon\Config\Adapter\Ini as ConfigIni;
use Phalcon\Mvc\Url;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new Loader();
		$loader->registerDirs(
		    array(
		    	//autoload base controller and model
		        // '../app/module/api/base/controller/',
		        // '../app/module/api/base/model/',

		        //autoload base model
		        // '../app/module/api/model/_base/',

		     	//autoload controller and model
		        // '../app/module/api/controller/',
		        // '../app/module/api/model/',

		        //Library
		        __DIR__ . '/library/Youtube/',
		    )
		);

		$loader->registerNamespaces(array(
			//register namespace for base controller and base model
			'App\Module\Api\Base\Controller' => '../app/module/api/base/controller/',
			'App\Module\Api\Base\Model' => '../app/module/api/base/model/',

			//register base model
			'App\Module\Api\Model\_Base' 		=> '../app/module/api/model/_base/',

			//register namespace for controller and model
			'App\Module\Api\Controller' => '../app/module/api/controller/',
			'App\Module\Api\Model' 		=> '../app/module/api/model/',

			'App\Module\Api\Library\GetLinkDownload' => __DIR__ . '/library/GetLinkDownload/',
		))->register();

		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices($di)
	{

		/**
		 * Read configuration
		 */
		$config = new ConfigIni(__DIR__ . "/config/config.ini");

		//Registering a dispatcher
		$di->set('dispatcher', function () {
			$dispatcher = new Dispatcher();
			$dispatcher->setDefaultNamespace("App\Module\Api\Controller");
			return $dispatcher;
		});

		//Registering the view component
		$di->set('view', function () {
			$view = new \Phalcon\Mvc\View(true);
			return $view;
		});

		$di->set('db', function () use ($config, $di) {

		    $connection =  new MySQLAdapter(array(
				'adapter' => $config->database->adapter,
				'host' => $config->database->host,
				'username' => $config->database->username,
				'password' => $config->database->password,
				'dbname' => $config->database->name,
				'charset' => $config->database->charset,
				'port'	=> $config->database->port,
			));
		    return $connection;			
		});

		//set uri for this module
		$di->set('url', function() {
		    $url = new \Phalcon\Mvc\Url();
		    $url->baseModulePath = __DIR__;
		    return $url;
		}, true);
	}
}
