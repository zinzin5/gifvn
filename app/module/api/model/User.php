<?php
namespace App\Module\Api\Model;
use App\Module\Api\Model\_Base\BaseUser as BaseUser;
use App\Library\File\FileHelper as FileHelper;

class User extends BaseUser
{
	public function toArray($extraData = null){
		$data = array();
		$data['id'] = $this->id;
		$data['name'] = $this->name;
		$data['avatar'] = $this->avatar;
		$data['date_create'] = $this->date_create;
		$data['type'] = $this->type;
		//get full url of user avatar
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$server_name = $_SERVER['HTTP_HOST'];
		if (strpos($this->avatar, 'http:') !== false ){
			$data['avatar'] = $this->avatar;
		}
		else{
			$data['avatar'] = ($this->avatar)? $protocol . $server_name . $this->di->get('url')->getBaseUri() . FileHelper::getUserAvatarFolderUrl(true) . $this->avatar: null;;
		}
		return $data;
	}
}
