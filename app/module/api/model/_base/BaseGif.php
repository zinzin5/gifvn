<?php
namespace App\Module\Api\Model\_Base;
use App\Module\Api\Base\Model\BaseModel as BaseModel;
class BaseGif extends BaseModel
{
	public $id;
	public $content;
	public $user_post;
	public $user_apply;
	public $date_create;
	public $gif;
	public $mp4;
	public $webm;
	public $size_gif;
	public $size_mp4;
	public $width;
	public $height;
	public $link_gif;
	public $state;
	public $poster;

	/**
	 * Data initializer
	 */
	public function initialize(){
		$this->setSource("gif");
	}
}
