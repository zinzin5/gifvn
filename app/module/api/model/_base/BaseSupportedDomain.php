<?php
namespace App\Module\Api\Model\_Base;
use App\Module\Api\Base\Model\BaseModel as BaseModel;
class BaseSupportedDomain extends BaseModel
{
	public $c_id;
	public $c_code;
	public $c_url;
	public $c_url_full_size;
	public $c_source_link;
	public $c_description;
	public $c_type;
	public $c_time_create;

	/**
	 * Data initializer
	 */
	public function initialize(){
		$this->setSource("tbl_supported_domain");
	}
}
