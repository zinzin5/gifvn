<?php
namespace App\Module\Api\Model\_Base;
use App\Module\Api\Base\Model\BaseModel as BaseModel;
class BaseUser extends BaseModel
{
	public $id;
	public $name;
	public $avatar;
	public $date_create;
	public $type;

	/**
	 * Data initializer
	 */
	public function initialize(){
		$this->setSource("user");
	}
}
