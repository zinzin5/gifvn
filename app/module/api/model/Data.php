<?php
namespace App\Module\Api\Model;
use App\Module\Api\Model\_Base\BaseData as BaseData;

class Data extends BaseData
{
	public function toArray($extraData = null){
		$data = array();
		$data['c_id'] = $this->c_id;
		$data['c_code'] = $this->c_code;
		$data['c_url'] = $this->c_url;
		$data['c_url_full_size'] = $this->c_url_full_size;
		$data['c_source_link'] = $this->c_source_link;
		$data['c_description'] = $this->c_description;
		$data['c_type'] = $this->c_type;
		$data['c_time_create'] = $this->c_time_create;
		if (($this->c_type == 'photo') && $extraData){
			$source_url_picture = 'https://graph.facebook.com/v2.3/' . $this->c_code . '/picture?width=500&height=500&' . $extraData;			
			if (strpos($data['c_url_full_size'],'graph.facebook.com') > -1){
				$data['c_url_full_size'] = $this->c_url;	
			}
		}
		return $data;
	}
}
