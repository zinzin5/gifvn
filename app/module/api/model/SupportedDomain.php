<?php
namespace App\Module\Api\Model;
use App\Module\Api\Model\_Base\BaseSupportedDomain as BaseSupportedDomain;

class SupportedDomain extends BaseSupportedDomain
{
	public function toArray($extraData = null){
		$data = array();
		$data['c_id'] = $this->c_id;
		$data['c_domain'] = $this->c_domain;
		$data['c_class'] = $this->c_class;
		$data['c_function'] = $this->c_function;
		$data['c_type_data'] = $this->c_type_data;
		$data['c_status'] = $this->c_status;
		return $data;
	}
}
