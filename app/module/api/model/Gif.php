<?php
namespace App\Module\Api\Model;
use App\Module\Api\Model\_Base\BaseGif as BaseGif;

class Gif extends BaseGif
{
	public function toArray($extraData = null){
		$data = array();
		$data['id'] = $this->id;
		$data['content'] = $this->content;
		$data['user_post'] = $this->user_post;
		$data['user_apply'] = $this->user_apply;
		$data['date_create'] = $this->date_create;
		$data['gif'] = $this->gif;
		$data['mp4'] = $this->mp4;
		$data['webm'] = $this->webm;
		$data['poster'] = $this->poster;
		$data['size_gif'] = $this->size_gif;
		$data['size_mp4'] = $this->size_mp4;
		$data['width'] = $this->width;
		$data['height'] = $this->height;
		$data['link_gif'] = $this->link_gif;
		return $data;
	}
}
