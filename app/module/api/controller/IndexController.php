<?php
namespace App\Module\Api\Controller;
//base controller
use App\Module\Api\Base\Controller\BaseController as BaseController;

//model
use App\Module\Api\Model\Data as Data;
use App\Module\Api\Model\SupportedDomain as SupportedDomain;

//library
use App\Library\HttpResponse\HttpResponse as HttpResponse;
use App\Library\Json\AjaxHelper as AjaxHelper;
//config
use App\Config\AppConstant as AppConstant;
class IndexController extends BaseController
{
	public function indexAction(){
		return	AjaxHelper::jsonSuccess(array(), 'Successfully retrieve data ');
	}
}