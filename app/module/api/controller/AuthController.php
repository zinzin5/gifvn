<?php
namespace App\Module\Api\Controller;

//base controller
use App\Module\Api\Base\Controller\BaseController as BaseController;

//model
use App\Module\Api\Model\User as User;

//library
use App\Library\HttpResponse\HttpResponse as HttpResponse;
use App\Library\Json\AjaxHelper as AjaxHelper;
use App\Library\Validator\Validator as Validator;
use App\Library\Security\SecurityHelper as SecurityHelper;
use App\Library\String\StringHelper as StringHelper;
use Facebook;

//config
use App\Config\AppConstant as AppConstant;

class AuthController extends BaseController
{
	/**
	 * [loginApiAction description]
	 * @return [json] [user information if success]
	 * [json]  [error status and message if fail]
	 */
	public function loginApiAction(){
		try{
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			if (isset($postParams->c_email) && !Validator::isEmpty($postParams->c_email)){
				$data['c_email'] = $postParams->c_email;
				$params['c_email'] = $data['c_email'];
			}
			else {
				HttpResponse::responseAuthenticationFailure();
				return	AjaxHelper::jsonError('Email cannot empty');
			}

			if (isset($postParams->c_password) && !Validator::isEmpty($postParams->c_password)){
				$data['c_password'] = SecurityHelper::encryptPassword($postParams->c_password);
				$params['c_password'] = $data['c_password'];
			}
			else {
				HttpResponse::responseAuthenticationFailure();
				return	AjaxHelper::jsonError('Password cannot empty');
			}

			//other condition
			$conditions = "c_email = :c_email: AND c_password = :c_password:";
			$user = User::findFirst(
			    array(
			        "conditions" => $conditions,
			        "bind" => $params
			    )
			);
			if($user){		
				$access_token = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
				$user->c_access_token = $access_token;
				$user->save();
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($user->toArray(), 'Login success');
			} else{
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError("User not found");	
			}
		}
		catch(Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/**
	 * [loginWithFacebookApiAction description]
	 * @return [json] [user information if success]
	 * [json]  [error status and message if fail]
	 */
	public function loginWithFacebookApiAction(){
		try{
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			if (isset($postParams->access_token) && !Validator::isEmpty($postParams->access_token)){
				$data['access_token'] = $postParams->access_token;
			}
			else {
				HttpResponse::responseAuthenticationFailure();
				return	AjaxHelper::jsonError('Access token cannot empty');
			}

			//get facebook information of user
			$fb = new Facebook\Facebook([
			  	'app_id' => '1033821006650583',
			  	'app_secret' => 'f961c3bfbe2242904fa0d4a87825df10',
			  	'default_graph_version' => 'v2.4',			  	
			]);
			$facebook_info = $fb->get('/me?fields=id,name,email', $data['access_token']);
			$facebook_user = $facebook_info->getGraphUser();
			if ($facebook_user){
				$data['facebook_id'] = $facebook_user['id'];
				$params['facebook_id'] = $data['facebook_id'];
			}
			//other condition
			$conditions = "c_facebook_id = :facebook_id:";
			$user = User::findFirst(
			    array(
			        "conditions" => $conditions,
			        "bind" => $params
			    )
			);

			if($user){		
				$access_token = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
				$user->c_access_token = $access_token;
				$user->save();
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($user->toArray(), 'Login success');
			}
			else{
				$user = new User();
				$user->c_facebook_id = $facebook_user['id'];
				$user->c_name = $facebook_user['name'];
				$user->c_avatar = "http://graph.facebook.com/{$user->c_facebook_id}/picture?type=large";
				$access_token = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
				$user->c_access_token = $access_token;
				$user->save();
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($user->toArray(), 'Signup success');
			}
		}
		catch(Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	public function testFacebookAuthAction(){
		try{
			session_start();
			$fb = new Facebook\Facebook([
			  	'app_id' => '1033821006650583',
			  	'app_secret' => 'f961c3bfbe2242904fa0d4a87825df10',
			  	'default_graph_version' => 'v2.4',
		  	]);
			$helper = $fb->getRedirectLoginHelper();
			$permissions = ['email', 'user_likes']; // optional
			$loginUrl = $helper->getLoginUrl('http://apiv2.zuilen.tk/auth/loginbyfacebookapi', $permissions);
			var_dump($loginUrl);die;
		}
		catch(Exeption $e){
			echo $e->getMessage();
		}
	}

	public function loginByFacebookApiAction(){
		session_start();
		// $fb = new Facebook\Facebook([
		//   	'app_id' => '1033821006650583',
		//   	'app_secret' => 'f961c3bfbe2242904fa0d4a87825df10',
		//   	'default_graph_version' => 'v2.4',
	 //  	]);
	  	$fb = new Facebook\Facebook([
		  	'app_id' => '1033821006650583',
		  	'app_secret' => 'f961c3bfbe2242904fa0d4a87825df10',
		  	'default_graph_version' => 'v2.4',
		  	// 'default_access_token' => 'CAAOsQTXc3NcBAOEOheBbJs6QeSIZCMRVZAVsppg4Sb6aQQC19SWAG1OP8ZBqUwH8UIo1jrpL6hTPAZCTgEl2ZCZBrsZBVbsGFeyWbolocy5xi8kFOiZBZBArlH4E5ZAKPPEU2R9shPZCnqNdE6ULdF1ws2GRZCquFcuKppZARxj8FW4H1nn7qtKMe3kmZBySoHmXvylzcZD'
		]);
		// $accessToken = 'CAAOsQTXc3NcBAOEOheBbJs6QeSIZCMRVZAVsppg4Sb6aQQC19SWAG1OP8ZBqUwH8UIo1jrpL6hTPAZCTgEl2ZCZBrsZBVbsGFeyWbolocy5xi8kFOiZBZBArlH4E5ZAKPPEU2R9shPZCnqNdE6ULdF1ws2GRZCquFcuKppZARxj8FW4H1nn7qtKMe3kmZBySoHmXvylzcZD';
		// $response = $fb->get('/me?fields=id,name,email', $accessToken);
		// $user = $response->getGraphUser();
		// var_dump($user['name']);die;
	  	$helper = $fb->getRedirectLoginHelper();
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		if (isset($accessToken)) {
		  	// Logged in!
		  	$_SESSION['facebook_access_token'] = (string) $accessToken;
		  	$accessToken = (string) $accessToken;
		  	$redis = new Redis();
		  	if ($redis->connect('127.0.0.1', 6379) && $redis->auth('zinzin')&& $redis->select(0)){
		  		$redis->set('authentication', (string) $accessToken);
		  	}
		  	var_dump($accessToken);die;
		  	$response = $fb->get('/me', $accessToken);
		  	$user = $response->getGraphUser();
		  	var_dump($user['name']);
		  	// Now you can redirect to another page and use the
		  	// access token from $_SESSION['facebook_access_token']
		}
	}
}