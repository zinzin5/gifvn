<?php
namespace App\Module\Api\Controller;
use App\Module\Api\Base\Controller\BaseController as BaseController;
class ErrorController extends BaseController
{
	public function error404Action()
	{
		echo "404 / Not found";
		die;
	}
}
