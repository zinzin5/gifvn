<?php
namespace App\Module\Api\Controller;
//base controller
use App\Module\Api\Base\Controller\BaseController as BaseController;

//model
use App\Module\Api\Model\Gif as Gif;
use App\Module\Api\Model\SupportedDomain as SupportedDomain;

//library
use App\Library\HttpResponse\HttpResponse as HttpResponse;
use App\Library\Json\AjaxHelper as AjaxHelper;
use App\Module\Api\Library\GetLinkDownload\Youtube;
use App\Module\Api\Library\GetLinkDownload\Facebook;

//config
use App\Config\AppConstant as AppConstant;

class GifController extends BaseController
{

	public function indexAction()
	{
		# code...
		return AjaxHelper::jsonSuccess(array(), "indexAction");
	}

	/*
	get newest data
	 */
	public function getNewGifAction(){
		try {
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$query = Gif::query();
			$params = array();

			//other condition
			$query->limit(AppConstant::OBJECT_PER_PAGE);
			$query->orderBy('id DESC');

			// echo(\Library\Falcon\Criteria_toSql::toSql($query->execute()));die;
			$list_data = $query->execute();

			$result = array();
			foreach ($list_data as $key => $value){
				array_push($result, $value->toArray());	
			}
			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/*
	get next data from a id
	 */
	public function getNextDataAction(){
		try {
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$query = Data::query();
			$params = array();
			
			//condition from input data ( POST data )
			if (isset($postParams->current_id) && !empty($postParams->current_id)){
				$data['current_id'] = $postParams->current_id;
				$query->andWhere("id > :current_id:");
				$params['current_id'] = $data['current_id'];
			}

			//other condition
			$query->limit(AppConstant::OBJECT_PER_PAGE);
			$query->bind($params);

			$list_data = $query->execute();
			$result = array();
			foreach ($list_data as $key => $value){
				array_unshift($result, $value->toArray());		
			}
			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/*
	get previous data from a id
	 */
	public function getPreviousDataAction(){
		try {
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$query = Data::query();
			$params = array();
			
			//condition from input data ( POST data )
			if (isset($postParams->current_id) && !empty($postParams->current_id)){
				$data['current_id'] = $postParams->current_id;
				$query->andWhere("c_id < :current_id:");
				$params['current_id'] = $data['current_id'];
			}
			
			//other condition
			$query->limit(AppConstant::OBJECT_PER_PAGE);
			$query->bind($params);
			$query->orderBy('id DESC');

			$list_data = $query->execute();
			$result = array();
			foreach ($list_data as $key => $value){
				array_push($result, $value->toArray());		
			}
			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/*
	get detail data from a id
	 */
	public function getDetailDataAction(){
		try {
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$query = Gif::query();
			$params = array();
			
			//condition from input data ( POST data )
			if (isset($postParams->id) && !empty($postParams->id)){
				$data['id'] = $postParams->id;
				$query->andWhere("id = :id:");
				$params['id'] = $data['id'];
			}
			else {
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError("Id of data cannot empty");	
			}
			
			//other condition
			$query->bind($params);
			$list_data = $query->execute();
			$result = $list_data[0]->toArray();

			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	public function uploadGifAction()
	{
		try {
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$query = Gif::query();
			
			//condition from input data ( POST data )
			if (isset($postParams->gif) && !empty($postParams->gif)){
				$url = $postParams->gif;
				$url = str_replace("http://", "", $url);
				$url = str_replace("https://", "", $url);
				$url = str_replace(".gif", "", $url);
				$postParams->gif = $url;
				$data['link_gif'] = $postParams->gif;
				$query->andWhere("link_gif = :link_gif:");
				$params['link_gif'] = $data['link_gif'];
			}
			else {
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError("gif of data cannot empty");	
			}
			
			//other condition
			$query->bind($params);
			$list_data = $query->execute();
			$result = array();
			if(count($list_data) > 0){
				$result = $list_data[0]->toArray();	
			}else{
				$json = file_get_contents("https://upload.gfycat.com/transcode?fetchUrl=" . $postParams->gif);
				$result = json_decode($json);
				if(isset($result->gfyName)){
					$json = file_get_contents("https://gfycat.com/cajax/get/" . $result->gfyName);
					$result = json_decode($json);
					$result = $result->gfyItem;
					if($result){
						$newGif = new Gif();
						if(isset($result->gifUrl)){
							$newGif->gif = $result->gifUrl;
						}
						if(isset($result->mp4Url)){
							$newGif->mp4 = $result->mp4Url;
						}
						if(isset($result->webmUrl)){
							$newGif->webm = $result->webmUrl;
						}
						if(isset($result->width)){
							$newGif->width = $result->width;
						}
						if(isset($result->height)){
							$newGif->height = $result->height;
						}
						if(isset($result->poster)){
							$newGif->poster = $result->posterUrl;
						}
						if(isset($result->gifSize)){
							$newGif->size_gif = $result->gifSize;
						}
						if(isset($result->mp4Size)){
							$newGif->size_mp4 = $result->mp4Size;
						}
						if(isset($postParams->content)){
							$newGif->content = $postParams->content;
						}
						$newGif->user_post = 3;
						$date = new \DateTime('now', new \DateTimeZone('Asia/Jakarta'));
						$newGif->date_create = $date->getTimestamp();
						$newGif->link_gif = $postParams->gif;
						// var_dump($newGif);
						if($newGif->save()){
							HttpResponse::responseOk();
							return	AjaxHelper::jsonSuccess($newGif, 'Successfully retrieve data ');
						}

					}
				}else{
					
				}
			}

			HttpResponse::responseOk();
			return	AjaxHelper::jsonSuccess($result, 'Successfully retrieve data ');
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}
}
