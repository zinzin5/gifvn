<?php
namespace App\Module\Api\Controller;

//base controller
use App\Module\Api\Base\Controller\BaseController as BaseController;

//model
use App\Module\Api\Model\User as User;
use App\Module\Api\Model\Data as Data;

//library
use App\Library\HttpResponse\HttpResponse as HttpResponse;
use App\Library\Json\AjaxHelper as AjaxHelper;
use App\Library\Validator\Validator as Validator;
use App\Library\Security\SecurityHelper as SecurityHelper;
use App\Library\String\StringHelper as StringHelper;
use App\Library\File\FileHelper as FileHelper;
use App\Library\ImageServiceStorage\Imgur as Imgur;

//config
use App\Config\AppConstant as AppConstant;

class UserController extends BaseController
{

	/**
	 * The function to check user session. We will check by email in session
	 * @param  [stdObject] $postParams [data send by request]
	 * @return [User]        [Null if not found. Otherwise, the corresponding user]
	 */
	public function checkAuth($postParams){
		//check authentication
		if (isset($postParams->access_token)){
			$data['access_token'] = $postParams->access_token;
			$params['access_token'] = $data['access_token'];
			$conditions = "access_token = :access_token:";
			$user = User::findFirst(
			    array(
			        "conditions" => $conditions,
			        "bind" => $params
			    )
			);

			if(!$user){
				return null;
			}
			else{
				return $user;
			}
		}
		else{
			return null;
		}
	}

	/**
	 * [loginApiAction description]
	 * @return [json] [user information if success]
	 * [json]  [error status and message if fail]
	 */
	public function loginAction(){
		try{
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			if (isset($postParams->email) && !Validator::isEmpty($postParams->email)){
				$data['email'] = $postParams->email;
				$params['email'] = $data['email'];
			}
			else {
				HttpResponse::responseAuthenticationFailure();
				return	AjaxHelper::jsonError('Email cannot empty');
			}

			if (isset($postParams->password) && !Validator::isEmpty($postParams->password)){
				$data['password'] = SecurityHelper::encryptPassword($postParams->password);
				$params['password'] = $data['password'];
			}
			else {
				HttpResponse::responseAuthenticationFailure();
				return	AjaxHelper::jsonError('Email cannot empty');
			}

			//other condition
			$conditions = "email = :email: AND password = :password:";
			$user = User::findFirst(
			    array(
			        "conditions" => $conditions,
			        "bind" => $params
			    )
			);

			if($user){		
				$access_token = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
				$user->access_token = $access_token;
				$user->save();
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($user->toArray(), 'Login success');
			}
			else{
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError("User not found");	
			}
		}
		catch(Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/**
	 * [getUserProfileApiAction description]
	 * @return [json] [user information if success]
	 * [json]  [error status and message if fail]
	 */
	public function fetchUserAction(){
		try{
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$requester = $this->checkAuth($postParams);

			if($requester){		
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($requester->toArray(), 'Get user profile success');
			}
			else{
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError('User not found');	
			}
		}
		catch(Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/**
	 * [createUserApiAction description]
	 * @return [json] [user information if success]
	 * [json] [error status and error message  if fail]
	 */
	public function createUserApiAction(){
		try{

			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$data = array();

			if (property_exists($postParams, 'name') && !Validator::isEmpty($postParams->name)){
				$data['name'] = $postParams->name;
			}

			if (property_exists($postParams, 'password') && !Validator::isEmpty($postParams->password)){
				$data['password'] = SecurityHelper::encryptPassword($postParams->password);
			}
			
			if (property_exists($postParams, 'email') && !Validator::isEmpty($postParams->email)){
				$data['email'] = $postParams->email;
			}

			if (property_exists($postParams, 'tel') && !Validator::isEmpty($postParams->tel)){
				$data['tel'] = $postParams->tel;
			}

            // Handle upload avatar for user
            foreach ($this->request->getUploadedFiles() as $file) {
            	if ($file->getKey() == 'avatar'){
            		if (in_array(strtolower($file->getExtension()), AppConstant::$imageExtensionAccept)){
		    			if (($file->getSize() > AppConstant::MAX_SIZE_IMAGE) || ($file->getSize() == 0)){
		        			HttpResponse::responseOk();
							return	AjaxHelper::jsonError('Avatar size is higher 2mb');
		    			}
		    			else {
		    				$random_unique_name = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
		            		if ($file->moveTo(FileHelper::getUserAvatarFolderUrl(true) . $random_unique_name . '.' . $file->getExtension())){
		            			$avatar = $random_unique_name . '.' . $file->getExtension();
		            			$data['avatar'] = $avatar;
		            		}
		            		else{
		            			HttpResponse::responseOk();
								return	AjaxHelper::jsonError('Cannot save avatar, please report for administrator');
		            		}	
		    			}            			
		    		}
		    		else{
		    			HttpResponse::responseOk();
						return	AjaxHelper::jsonError('Extension of avatar is not correct.');	
		    		}
            	}    	
            }

			$access_token = StringHelper::generateRandomString(AppConstant::ACCESS_TOKEN_LENGTH);
			$data['access_token'] = $access_token;

			$user = new User();
			foreach ($data as $key => $value){
				$user->$key = $value;				
			}
			$save = $user->save();

			if($save){
				HttpResponse::responseOk();
				return	AjaxHelper::jsonSuccess($user->toArray(null), 'Successfully create new user');
			}
			else{
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError('Fail to create new user!');
			}
		}
		catch (Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}

	/**
	 * [userUploadImageAction description]
	 * @return [json] [image information]
	 * [json] [error status and error message if fail]
	 */
	public function userUploadImageApiAction(){
		try{
			$postData = json_encode($_POST);
			$postParams = json_decode($postData);

			$requester = $this->checkAuth($postParams);
			if (!$requester){
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError('You must login before upload image');	
			}

			if (property_exists($postParams, 'description') && !Validator::isEmpty($postParams->description)){
				$data['description'] = $postParams->description;
			} else{
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError('You must have a title for image');		
			}

			$files = $this->request->getUploadedFiles();
			if (count($files) > 1){
				HttpResponse::responseOk();
				return	AjaxHelper::jsonError('You can upload only one image one time');			
			}

            // Handle upload avatar for user
            foreach ($files as $file) {
        		if (in_array(strtolower($file->getExtension()), AppConstant::$imageExtensionAccept)){
	    			if (($file->getSize() > AppConstant::MAX_SIZE_IMAGE) || ($file->getSize() == 0)){
	        			HttpResponse::responseOk();
						return	AjaxHelper::jsonError('Image size is higher 1mb');
	    			}
	    			else {
	    				$imgur = new Imgur();
	    				$upload_result = $imgur->uploadImage($file);	
	    				if ($upload_result['error'] == 1){
	    					HttpResponse::responseOk();
							return	AjaxHelper::jsonError($upload_result['message']);	
	    				} else{
	    					$content = new Data();
	    					$content->code = $upload_result['url'];
	    					$content->url = $upload_result['url'];
	    					$content->url_full_size = $upload_result['url'];
	    					$content->source_link = $upload_result['url'];
	    					$content->type = 'photo';
	    					$content->description = $data['description'];
	    					$content->status = 1;
	    					$content->time_create = time();
	    					if ($content->save()){
	    						HttpResponse::responseOk();
								return	AjaxHelper::jsonSuccess($content->toArray(null), 'Upload image successfully');
	    					} else{
	    						HttpResponse::responseOk();
								return	AjaxHelper::jsonError('Have a problem, please try again');	
	    					}
	    				}
	    			}            			
	    		} else{
	    			HttpResponse::responseOk();
					return	AjaxHelper::jsonError('Extension of image is not correct.');	
	    		} 	
            }
		}
		catch(Exception $e){
			HttpResponse::responseOk();
			return	AjaxHelper::jsonError($e->getMessage());
		}
	}
}