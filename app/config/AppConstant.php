<?php
namespace App\Config;
	class AppConstant
	{
		const OBJECT_PER_PAGE = 20;
		const APP_NAME = 'Zinzin';
		const ACCESS_TOKEN_LENGTH = 25;
		const DATETIME_FORMAT = 'Y/m/d H:i:s';
		const MAX_SIZE_IMAGE = '1048000';		//	1 * 1024 * 1024 unit: bytes
		const TIME_EXPIRED_YOUTUBE = 14400;
		public static $imageExtensionAccept = array('jpg', 'png', 'gif', 'jpeg');
	}
?>