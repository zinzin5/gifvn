<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: X-Accept-Charset,X-Accept,Content-Type');

error_reporting(E_ALL);

use Phalcon\Loader;
use Phalcon\Mvc\Router;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Application as BaseApplication;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Dispatcher\Exception as DispatchException;

//Add facebook sdk
require_once __DIR__ . '/../app/library/FacebookSDK/autoload.php';

class Application extends BaseApplication
{

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	protected function registerServices()
	{

		$di = new FactoryDefault();

		$loader = new Loader();

		/**
		 * We're a registering a set of directories taken from the configuration file
		 */
		$loader->registerDirs(
			array(
				//config
				// __DIR__ . '/../app/config/',
			)
		)->register();

		$loader->registerNamespaces(array(
			'App\Config' => __DIR__ . '/../app/config/',
			'App\Library\HttpResponse' => __DIR__ . '/../app/library/HttpResponse/',
			'App\Library\Json' => __DIR__ . '/../app/library/Json/',
			'App\Library\Validator' => __DIR__ . '/../app/library/Validator/',
			'App\Library\String' => __DIR__ . '/../app/library/String/',
			'App\Library\File' => __DIR__ . '/../app/library/File/',
			'App\Library\Image' => __DIR__ . '/../app/library/Image/',
			'App\Library\Security' => __DIR__ . '/../app/library/Security/',
			'App\Library\ImageServiceStorage' => __DIR__ . '/../app/library/ImageServiceStorage/',
		))->register();

		//Registering a router
		$di->set('router', function(){

			$router = new Router();
			$router->removeExtraSlashes(true);

			//route for api module
			$router->setDefaultModule("api");
			$router->add('/:controller/:action', array(
				'module' => 'api',
				'controller' => 1,
				'action' => 2,
			));
			
			$router->add('/manager/:controller/:action', array(
				'module' => 'manager',
				'controller' => 1,
				'action' => 2,
			));

			$router->add('/:module', array(
				'module' => 1,
				'controller' => "index",
				'action' => "index",
			));

			$router->notFound([
			    "module" => "error",
			    "controller" => 'error',
			    "action"     => 'error404',
			]);

			return $router;
		});

		$di->set('dispatcher', function(){
		    // Create an EventsManager
		    
		    $eventsManager = new EventsManager();
		    $eventsManager->attach("dispatch:beforeException", function ($event, $dispatcher, $exception) {
		        switch ($exception->getCode()) {
		            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
		            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
		                $dispatcher->forward(
		                    array(
		                    	'module' => 'api',
		                        'controller' => 'error',
		                        'action'     => 'error404'
		                    )
		                );

		                return false;
		        }
		    });
	        $dispatcher = new MvcDispatcher();
		    // Bind the eventsManager to the view component
		    $dispatcher->setEventsManager($eventsManager);

		    return $dispatcher;
		});

		$this->setDI($di);
	}



	public function main()
	{

		$this->registerServices();

		//Register the installed module
		$this->registerModules(array(
			'api' => array(
				'className' => 'App\Module\Api\Module',
				'path' => '../app/module/api/Module.php'
			),
			'manager' => array(
				'className' => 'App\Module\Manager\Module',
				'path' => '../app/module/manager/Module.php'
			),
			'error' => array(
				'className' => 'App\Module\Error\Module',
				'path' => '../app/module/error/Module.php'
			)
		));
		echo $this->handle()->getContent();
	}

}

$appslication = new Application();

//catch exception when module or controller or action is incorrect
try{
	$appslication->main();	
}
catch(Exception $e){
	$result['error'] = 1;
	$result['message'] = $e->getMessage();
	App\Library\HttpResponse\HttpResponse::responseOk();
	return	App\Library\Json\AjaxHelper::jsonError($e->getMessage());
}


